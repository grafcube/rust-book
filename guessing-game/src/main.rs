use rand::Rng;
use std::{
    cmp::Ordering,
    io::{self, Write},
};

fn main() {
    println!("Guess the number!!!");

    let winner = rand::thread_rng().gen_range(1..=180);

    loop {
        print!("\nYour guess: ");
        io::stdout().flush().unwrap();
        let guess = read_guess();
        println!("You guessed '{guess}'!");

        match guess.cmp(&winner) {
            Ordering::Less => println!("Missed :( Too low!"),
            Ordering::Greater => println!("Missed :( Too high!"),
            Ordering::Equal => {
                println!("Hit!!! You got it right!");
                return;
            }
        }
    }
}

fn read_guess() -> u32 {
    let mut guess = String::new();
    loop {
        io::stdin().read_line(&mut guess).unwrap();
        // https://old.reddit.com/r/rust/comments/obnlv8/some_neat_rust_syntax_loop_break_match/
        break match guess.trim().parse() {
            Err(_) => {
                println!("Enter a valid number");
                continue;
            }
            Ok(i) => i,
        };
    }
}
